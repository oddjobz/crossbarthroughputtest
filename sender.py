#!/usr/bin/env python3
from autobahn.twisted.component import run
from twisted.internet.defer import DeferredList, inlineCallbacks
from autobahn.twisted.component import Component
import time


@inlineCallbacks
def onJoin(session, details):
    print(f'Join session "{session}" => ', details)

    tot = 0
    beg = time.time()
    for item in range(40 * 22):
        buf = yield session.call('crossbar.benchmark.endpoint')
        tot += len(buf)
    end = time.time()
    print('1. Duration = ', end - beg)
    print(f'1. Total transfer = {tot} bytes, rate = {int(tot/(end-beg)/1024)}K/s')

    deferreds = []
    beg = time.time()
    for item in range(40 * 22):
        deferreds.append(session.call('crossbar.benchmark.endpoint'))

    def results(result):
        tot = 0
        for e, (success, value) in enumerate(result):
            if success:
                tot += len(value)
            else:
                print('Error:', value.getErrorMessage())

        end = time.time()
        print('2. Duration = ', end - beg)
        print(f'2. Total transfer = {tot} bytes, rate = {int(tot/(end-beg)/1024)}K/s')

    dlist = DeferredList(deferreds)
    dlist.addCallback(results)


component = Component(
            transports=[{'url': 'ws://localhost:8080/ws'}],
            realm='realm1',
        )
component.on('join', onJoin)
run([component])
