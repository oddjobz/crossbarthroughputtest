all:
	@echo Targets are run_crossbar, run_listener and run_sender
	@echo OR just do \"make go\" for everything.

run_crossbar:
	~/tmp/wtf/crossbarfx/dist/crossbarfx edge init
	~/tmp/wtf/crossbarfx/dist/crossbarfx edge start

run_listener:
	./listener.py &

run_sender:
	./sender.py

go: 
	make run_crossbar &
	sleep 10
	make run_listener &
	sleep 2
	make run_sender
