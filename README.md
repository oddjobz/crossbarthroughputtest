# Basic Crossbar thruput tester ..

```shell
mkvirtualenv -p /usr/bin/python3 tests
pipenv install
make go
```
Check the throughput, then get source repo's for;
* autobahn

And do a;
```
make install
```
From within your virtualenv .. then stop listener and sender, and re-run.
Latest figure on my workstation with a single listener;
```
2019-02-24T17:50:03+0000 Total transfer = 230686720 bytes, rate = 82359K/s
```
Currently the listener will print DEBUG to prove it's really working, so don't run this in
a PyCharm or VS console, use a proper xterm. (the latter won't slow it down)
..
You can now do;
```
make go
```
To run the entire test .. 

TODO: make the sender exit after one pass.