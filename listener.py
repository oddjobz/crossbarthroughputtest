#!/usr/bin/env python3
from autobahn.twisted.component import run
from autobahn.wamp.types import RegisterOptions
from twisted.internet.defer import inlineCallbacks
from autobahn.twisted.component import Component

count = 0
total = 0


def benchmark_endpoint(*args, details=None):
    global count
    global total
    count += 1
    total += 1024 * 256
    print('> ', count, total)
    return b' ' * 1024 * 256


@inlineCallbacks
def onJoin(session, details):
    print(f'Join session "{session}" => ', details)
    options = RegisterOptions(details_arg='details', invoke=u'roundrobin')
    x = yield session.register(benchmark_endpoint, 'crossbar.benchmark.endpoint', options)


component = Component(
            transports=[{'url': 'ws://localhost:8080/ws'}],
            realm='realm1',
        )
component.on('join', onJoin)
run([component])
